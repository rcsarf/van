import { createStackNavigator, createAppContainer } from 'react-navigation';
import Main from './pages/main';
const MainNavigator = createStackNavigator({
        Main
},{
        defaultNavigationOptions: {
                headerStyle: {
                backgroundColor: "#DA552F"
                },
                headerTintColor: "white"
        }
});
export default createAppContainer(MainNavigator);